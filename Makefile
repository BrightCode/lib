##
## Makefile for Makefile in /home/guzman_y/rendu/Piscine_C_J11
##
## Made by yoann guzman
## Login   <guzman_y@epitech.net>
##
## Started on  Mon Oct 20 14:22:15 2014 yoann guzman
## Last update Mon Jul 27 16:08:59 2015 yoann guzman
##

RM	=	rm -f

HEAD	=	bc.h

CFLAGS	+=	-Wextra -Wall -Werror
CFLAGS	+=	-ansi -pedantic
CFLAGS	+=	-I../include

NAME	=	libbc$(HOSTTYPE).a

LIB	=	ar rc

SRCS	=	bc_get_char.c \
		bc_get_line.c \
		bc_write_char.c \
		bc_write_nb.c \
		bc_write_str.c \
		bc_init_rand.c \
		bc_rand.c

OBJS	=	$(SRCS:.c=.o)

all	:	$(NAME)

$(NAME)	:	$(OBJS)
		$(LIB) $(NAME) $(OBJS)
		ranlib $(NAME)

clean	:
		$(RM) $(OBJS)

fclean	:	clean
		$(RM) $(NAME)

re	:	fclean all

.PHONY	:	all clean fclean re
