#include <unistd.h>

char	*bc_get_line(int);

char	bc_get_char(void)
{
  char	*str;

  str = NULL;
  str = bc_get_line(0);
  if (str == NULL)
    return (0);
  return (str[0]);
}
