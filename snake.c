#include "Snake.h"
#include <stdio.h>

int mv_snake(int **tab, Direction dir)
{
  int rep;
  int i;
  int j;
  int lg=0;
  int iposdeb;
  int jposdeb;
  int iposfin;
  int jposfin;
  for(i=0; i<100; i++)
    for(j=0; j<30; j++)
      if (tab[i][j]>0)
	lg++;
  for(i=0; i<100; i++)
    for(j=0; j<30; j++)
      if (tab[i][j]==1)
	{
	  iposdeb=i;
	  jposdeb=j;
	}
  for(i=0; i<100; i++)
    for(j=0; j<30; j++)
      if (tab[i][j]==lg)
	{
	  iposfin=i;
	  jposfin=j;
	}
  if((jposdeb==0)&&(dir==UP))
    return(-1);
  else if((jposdeb==29)&&(dir==DOWN))
    return(-1);
  else if((iposdeb==0)&&(dir==LEFT))
    return(-1);
  else if((iposdeb==99)&&(dir==RIGHT))
    return(-1);
  
  else if((dir==LEFT)&&(tab[iposdeb-1][jposdeb]>1))
    return(-1);
  else if((dir==RIGHT)&&(tab[iposdeb+1][jposdeb]>1))
    return(-1);
  else if((dir==UP)&&(tab[iposdeb][jposdeb-1]>1))
    return(-1);
  else if((dir==DOWN)&&(tab[iposdeb][jposdeb+1]>1))
    return(-1);
  
  else if((dir==DOWN)&&(tab[iposdeb][jposdeb+1]==0))
    {
      for(i=0; i<100; i++)
	for(j=0; j<30; j++)
	  if (tab[i][j]>0)
	    tab[i][j]++;
      tab[iposdeb][jposdeb+1]=1;
      tab[iposfin][jposfin]=0;
      return(0);
    }
  else if((dir==UP)&&(tab[iposdeb][jposdeb-1]==0))
    {
      for(i=0; i<100; i++)
	for(j=0; j<30; j++)
	  if (tab[i][j]>0)
	    tab[i][j]++;
      tab[iposdeb][jposdeb-1]=1;
      tab[iposfin][jposfin]=0;
      return(0);
    }
  else if((dir==RIGHT)&&(tab[iposdeb+1][jposdeb]==0))
    {
      for(i=0; i<100; i++)
	for(j=0; j<30; j++)
	  if (tab[i][j]>0)
	    tab[i][j]++;
      tab[iposdeb+1][jposdeb]=1;
      tab[iposfin][jposfin]=0;
      return(0);
    }
  else if((dir==LEFT)&&(tab[iposdeb-1][jposdeb]==0))
    {
      for(i=0; i<100; i++)
	for(j=0; j<30; j++)
	  if (tab[i][j]>0)
	    tab[i][j]++;
      tab[iposdeb-1][jposdeb]=1;
      tab[iposfin][jposfin]=0;
      return(0);
    }
  
  else if((dir==DOWN)&&(tab[iposdeb][jposdeb+1]==-1))
    {
      for(i=0; i<100; i++)
	for(j=0; j<30; j++)
	  if (tab[i][j]>0)
	    tab[i][j]++;
      tab[iposdeb][jposdeb+1]=1;
      return(1);
    }
  else if((dir==UP)&&(tab[iposdeb][jposdeb-1]==-1))
    {
      for(i=0; i<100; i++)
	for(j=0; j<30; j++)
	  if (tab[i][j]>0)
	    tab[i][j]++;
      tab[iposdeb][jposdeb-1]=1;
      return(1);
    }
  else if((dir==RIGHT)&&(tab[iposdeb+1][jposdeb]==-1))
    {
      for(i=0; i<100; i++)
	for(j=0; j<30; j++)
	  if (tab[i][j]>0)
	    tab[i][j]++;
      tab[iposdeb+1][jposdeb]=1;
      return(1);
    }
  else
    {
      for(i=0; i<100; i++)
	for(j=0; j<30; j++)
	  if (tab[i][j]>0)
	    tab[i][j]++;
      tab[iposdeb-1][jposdeb]=1;
      return(0);
    }	
}
