#include <unistd.h>

void	bc_write_nb(int nb)
{
  int	n;
  int	pow;
  char	c;

  pow = 1;
  if (nb < 0)
    {
      write(1, "-", 1);
      nb *= -1;
    }
  while (pow <= nb)
    pow *= 10;
  if (pow > 1)
    pow = pow / 10;
  while (pow > 0)
    {
      n = nb / pow;
      nb = nb % pow;
      pow = pow / 10;
      c = n + '0';
      write(1, &c, 1);
    }
}
